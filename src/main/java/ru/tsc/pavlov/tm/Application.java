package ru.tsc.pavlov.tm;

import ru.tsc.pavlov.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
